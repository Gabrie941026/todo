import React, {useState} from 'react'

function Todolist({todo, setTodo}){
    const [edit, setEdit] = useState(null)
    const [value, setValue] = useState('')
    const [file, setFile] = useState()
    const [date, setDate] = useState()


    //фуекция для удалене задач
    function deleteTodo(id){
        let newTodo = [...todo].filter(item => item.id!=id)
        setTodo(newTodo)
    }

//фуекция которая поквзывает статуc задачи true/falce
    function statusTodo(id){
        let newTodo = [...todo].filter(item => {
            if(item.id==id){
                item.status = !item.status
            }
            return item
        })
        setTodo(newTodo)
    }

//фуекция для редактироване задач
  function editTodo(id, title, ){
        setEdit(id)
        setValue(title)
       
  }

  //фуекция для сохроненя задач
  function saveTodo(id){
let newTodo = [...todo].map(item => {
    if(item.id == id){
        item.title = value;
        item.date = date
        
    }
   return item
})
setTodo(newTodo)
setEdit(null)

  }
//фуекция для загруки файлов
  function handleChange(event) {
    setFile(event.target.files[0])
  }

return(
    <div>
        {
           todo.map( item=> (
            <div key={item.id}>
                {
                    edit == item.id ? 
                    <div>
                    <input  value={value} onChange={(e)=> setValue(e.target.value)} />
                    <input type="date" date={date} onChange ={ (e)=>setDate(e.target.date)}   />
                    </div>:
                    <div>{item.title}  {item.date}     
                    </div>
                }
            {
                  edit == item.id ? 
                  <div>
                        <button onClick={()=> saveTodo(item.id)} >Save</button>
                        
                  </div> :
                  <div>
                        
                        <input type="file" onChange={handleChange}/>
                        <button onClick={()=>deleteTodo(item.id)}>Remove</button>
                        <button onClick={()=>statusTodo(item.id)}>Close/Open</button>
                        <button onClick={()=>editTodo(item.id, item.title)}>Edit</button>
                  </div>
            }
            </div>
           ))
        }
    </div>
)
}
export default Todolist