

import React, { useState } from 'react'
import { v1 as uuidv1 } from 'uuid';

function AddTodo ({todo, setTodo}){

const [value, setValue, date, setDate] = useState ('')


//здесь мы создоем обект уникальный id назване статус и дату
function saveTodo(){
setTodo(
    [...todo, {
        id: uuidv1(),
        title: value,
        status: true,
        date: date
     
    }]

)
setValue('');
setDate('');

}


    return(
        <div>
          
            <input type="date" date={date} onChange ={ (e)=>setDate(e.target.date)}   />


            <input placeholder="Add text" value={value} onChange ={ (e)=>setValue(e.target.value)} />
            <button onClick={saveTodo}>Save</button>
        </div>
    )
}
export default AddTodo
