import React, { useState } from 'react'
import './App.css';
import Header from './components/Header/Header';
import AddTodo from './components/Todolist/AddTodo';
import Todolist from './components/Todolist/Todolist';


//это нащ изначальный обект который показывает после загрузки
function App() {

  const [todo, setTodo] = useState([
    {
      id: 1,
      title: 'first todo',
      status: true,
      date: '22/10/2000',
    },
    {
      id: 2,
      title: 'second todo',
      status: true,
      date: '22/10/2000',
    },
    {
      id: 3,
      title: 'third todo',
      status: false,
      date: '22/10/2000'
    }

  ])





  return (
    <div className="App">
   <Header />
   <AddTodo todo={todo} setTodo={setTodo} />
   <Todolist todo={todo} setTodo={setTodo}   />
    </div>
  );
}

export default App;
